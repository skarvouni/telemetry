#ifndef COMPARELAPSWIDGET_H
#define COMPARELAPSWIDGET_H

#include "CompareTelemetryWidget.h"

#include <QTreeWidget>


class CompareLapsWidget : public CompareTelemetryWidget
{
	Q_OBJECT

  public:
	CompareLapsWidget(QWidget *parent = nullptr);
	~CompareLapsWidget() override = default;

  public slots:
	void browseData() override;
	void openDataFiles(const QStringList &filenames) override;

  protected:
	void fillInfoTree(InfoTreeWidget *tree, const TelemetryData *data) override;
	bool showTrackTurns() const override { return true; }
};

#endif // COMPARELAPSWIDGET_H
