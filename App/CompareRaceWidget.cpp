#include "CompareRaceWidget.h"

#include "Core/Race.h"

#include <InfoTreeWidget.h>
#include <QFileDialog>
#include <QHeaderView>
#include <QtDebug>

#include <algorithm>

#include <Core/UdpDataCatalog.h>


CompareRaceWidget::CompareRaceWidget(QWidget *parent) : CompareTelemetryWidget(" lap", "", parent)
{
	setDataName("Race");
}

void CompareRaceWidget::browseData()
{
	auto filenames = QFileDialog::getOpenFileNames(this, "Select some stints to compare", "", "*.f1race", nullptr,
												   QFileDialog::DontUseNativeDialog);

	openDataFiles(filenames);
}

void CompareRaceWidget::openDataFiles(const QStringList &filenames)
{
	QVector<TelemetryData *> races;
	for(auto file : filenames) {
		if(file.endsWith(".f1race")) {
			races.append(Race::fromFile(file));
		}
	}

	addTelemetryData(races);
}

void CompareRaceWidget::fillInfoTree(InfoTreeWidget *tree, const TelemetryData *data)
{
	auto race = dynamic_cast<const Race *>(data);
	if(!race)
		return;

	tree->clear();

	driverItem(tree, race);
	trackItem(tree, race);

	auto nbStints = qMin(race->stintsLaps.count(), race->stintsVisualTyre.count());
	auto stintsItem = tree->makeTopLevelItem("Stints", QString::number(nbStints));
	if(std::any_of(race->linkedFiles().begin(), race->linkedFiles().end(),
				   [](const auto &dataFile) { return QFile::exists(dataFile); })) {
		tree->setItemAction(stintsItem, "Open All Laps", [this, race]() { emit askOpenStints(race->linkedFiles()); });
	}

	for(int stintIndex = 0; stintIndex < nbStints; ++stintIndex) {
		auto compound = UdpDataCatalog::visualTyre(race->stintsVisualTyre.value(stintIndex));
		auto stintDesc = QString("%1 (%2 Laps)").arg(compound).arg(race->stintsLaps.value(stintIndex));
		auto stint = tree->makeItem(stintsItem, "Stint " + QString::number(stintIndex + 1), stintDesc);
		if(QFile::exists(race->linkedFiles().value(stintIndex))) {
			tree->setItemAction(stint, "Open lap", [stintIndex, this, race]() {
				emit askOpenStints({race->linkedFiles().value(stintIndex)});
			});
		}
	}
	tree->expandItem(stintsItem);

	auto nbPitstops = race->pitstops.count();
	auto pitstopItem = new QTreeWidgetItem(tree, {"Pitstops", QString::number(nbPitstops)});
	for(int i = 0; i < nbPitstops; ++i) {
		new QTreeWidgetItem(pitstopItem,
							{"Pitstop " + QString::number(i + 1), QString::number(race->pitstops.value(i)) + "s"});
	}
	tree->expandItem(pitstopItem);

	auto raceResultItem = new QTreeWidgetItem(tree, {"Race Result", QString::number(race->endPosition)});
	new QTreeWidgetItem(raceResultItem, {"Started Position", QString::number(race->startedGridPosition)});
	new QTreeWidgetItem(raceResultItem, {"Safety Cars", QString::number(race->nbSafetyCars)});
	new QTreeWidgetItem(raceResultItem, {"Virtual Safety Cars", QString::number(race->nbVirtualSafetyCars)});
	new QTreeWidgetItem(raceResultItem, {"Total Pernalties", QString::number(race->penalties) + "s"});
	new QTreeWidgetItem(raceResultItem, {"Points Scored", QString::number(race->pointScored)});
	new QTreeWidgetItem(raceResultItem, {"Race Status", UdpDataCatalog::raceStatus(race->raceStatus)});
	tree->expandItem(raceResultItem);

	auto fuel = race->fuelOnStart - race->fuelOnEnd;
	auto fuelItem =
		new QTreeWidgetItem(tree, {"Average Fuel Consumption", QString::number(fuel / race->nbLaps()) + "kg"});
	new QTreeWidgetItem(fuelItem, {"Start", QString::number(race->fuelOnStart) + "kg"});
	new QTreeWidgetItem(fuelItem, {"End", QString::number(race->fuelOnEnd) + "kg"});

	engineItem(tree, race);
	setupItem(tree, race);

	coastingItem(tree, race);

	eventsItem(tree, race);

	flashbackItem(tree, race);
	recordItem(tree, race);

	tree->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
}
