#ifndef COMPARETELEMETRYPAGE_H
#define COMPARETELEMETRYPAGE_H

#include <CompareWidgetInterface.h>
#include <QBoxLayout>
#include <QChart>
#include <QSignalMapper>
#include <QToolBar>
#include <QWidget>

class TelemetryChartView;

struct PageSettings {
	QString name;
	QStringList variables;
	bool detached = false;

	bool isValid() const { return !name.isEmpty(); }
};

class CompareTelemetryPage : public QWidget
{
	Q_OBJECT

  signals:
	void chartDataChanged();
	void detachAsked(bool value);
	void renameAsked();
	void saveAsked();
	void deleteAsked();
	void closed();
	void zoomModeChanged(QChartView::RubberBand zoomMode);

  public:
	explicit CompareTelemetryPage(const QString &name, const QString &dataName, QWidget *parent = nullptr);

	void addWidget(CompareWidgetInterface *widget, const QStringList &varOrder = QStringList());
	void removeWidget(CompareWidgetInterface *widget);

	void alignCharts();
	void updateChartMargin(CompareWidgetInterface *widget) const;

	const QList<CompareWidgetInterface *> &widgets() const;

	void moveGraph(CompareWidgetInterface *widget, int nb);
	bool canMoveGraph(CompareWidgetInterface *widget, int nb);

	void clear();

	void highlight(int lapIndex);

	QString name() const;
	void setName(const QString &name);

	void detach();
	void setDetached(bool value);
	bool isDetached() const;

	QStringList variables() const;
	int totalStretch() const;

	std::shared_ptr<PageSettings> pageSettings() const;
	void setPageSettings(const std::shared_ptr<PageSettings> &value);

	void setZoomMode(QChartView::RubberBand zoomMode);

  private:
	bool _detached = false;
	QString _name;
	QString _dataName;
	QList<CompareWidgetInterface *> _widgets;
	QVBoxLayout *_layout;
	int _chartMargins;
	QToolBar *_toolbar;
	QAction *_detachAction;
	int _totalStretch = 0;
	std::shared_ptr<PageSettings> _pageSettings = nullptr;

	bool _selectionHighlighted = true;
	int _currentHighlightedLapIndex = -1;

	QAction *_saveAction;
	QAction *_renameAction;
	QAction *_deleteAction;
	QAction *_zoomAction;
	QAction *_zoomHAction;
	QAction *_zoomVAction;

	QChartView::RubberBand _currentZoomMode = QChartView::RectangleRubberBand;

	void changeWidgetsZoomMode(QChartView::RubberBand zoomMode);

  protected:
	void closeEvent(QCloseEvent *event) override;

  public slots:
	void refreshHighlighting();

  private slots:
	void setZoomRect();
	void setZoomVertical();
	void setZoomHorizontal();
};

#endif // COMPARETELEMETRYPAGE_H
