#include "UdpDebugger.h"
#include "ui_UdpDebugger.h"

#include <QAction>
#include <QMenu>
#include <QTimer>

UdpDebugger::UdpDebugger(UdpLogger *logger, QWidget *parent)
: QDialog(parent, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), ui(new Ui::UdpDebugger)
{
	ui->setupUi(this);
	ui->spRefreshRate->setValue(300);

	_model = new DebuggerTableModel(logger, this);
	ui->tableView->setModel(_model);

	ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui->tableView->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui->tableView->horizontalHeader(), &QWidget::customContextMenuRequested, this,
			&UdpDebugger::variableContextMenuRequested);

	connect(ui->checkWriteToDisk, &QCheckBox::toggled, this, &UdpDebugger::writeToDisk);
	connect(ui->spRefreshRate, qOverload<int>(&QSpinBox::valueChanged), this, &UdpDebugger::changeRefreshRate);
	connect(ui->btnStartStop, &QPushButton::clicked, this, &UdpDebugger::startStop);

	updateStartStopText();
	_model->refresh();
}

UdpDebugger::~UdpDebugger() { delete ui; }

void UdpDebugger::startAutoRefresh(int timeoutInMs)
{
	if(timeoutInMs > 0) {
		if(!_timer) {
			_timer = new QTimer(this);
			connect(_timer, &QTimer::timeout, _model, &DebuggerTableModel::refresh);
		}

		_timer->setInterval(timeoutInMs);
		_timer->start();
	} else {
		delete _timer;
		_timer = nullptr;

		connect(_model->logger(), &UdpLogger::updated, _model, &DebuggerTableModel::refresh);
	}

	_isStarted = true;
}

void UdpDebugger::stopAutoRefresh()
{
	if(_isStarted) {
		if(_timer) {
			_timer->stop();
		} else {
			disconnect(_model->logger(), &UdpLogger::updated, _model, &DebuggerTableModel::refresh);
		}
	}

	_isStarted = false;

	_model->logger()->writeBuffers();
}

void UdpDebugger::updateStartStopText() { ui->btnStartStop->setText(!_isStarted ? "Start" : "Stop"); }

void UdpDebugger::startStop()
{
	if(_isStarted) {
		stopAutoRefresh();
	} else {
		startAutoRefresh(ui->spRefreshRate->value());
	}

	updateStartStopText();
}

void UdpDebugger::writeToDisk(bool value) { _model->logger()->setWriteOnDisk(value); }

void UdpDebugger::changeRefreshRate(int value)
{
	if(_isStarted)
		startAutoRefresh(value);
}

void UdpDebugger::variableContextMenuRequested(const QPoint &pos)
{
	QMenu menu;
	const auto &allvariables = _model->logger()->availableVariables();
	for(const auto &variable : allvariables) {
		auto name = _model->logger()->variableName(variable);
		auto isChecked = _model->logger()->variables().contains(variable);
		auto action = menu.addAction(name);
		action->setCheckable(true);
		action->setChecked(isChecked);
		action->setData(variable);
		connect(action, &QAction::toggled, this, &UdpDebugger::variableChecked);
	}

	if(!menu.isEmpty()) {
		menu.exec(mapToGlobal(pos));
	}
}

void UdpDebugger::variableChecked(bool value)
{
	auto action = qobject_cast<QAction *>(sender());
	if(action) {
		auto variable = static_cast<LoggableVariable>(action->data().toInt());
		_model->logger()->setVariable(variable, value);
		_model->refresh();
	}
}

// ------------------------------------------------------------------------------------------------------------------------------

DebuggerTableModel::DebuggerTableModel(UdpLogger *logger, QObject *parent)
: QAbstractTableModel(parent), _logger(logger)
{
}

int DebuggerTableModel::rowCount(const QModelIndex &) const { return _logger->trackedIndexes().count(); }

int DebuggerTableModel::columnCount(const QModelIndex &) const { return _logger->variables().count(); }

QVariant DebuggerTableModel::data(const QModelIndex &index, int role) const
{
	auto data = QVariant();
	if(!index.isValid())
		return data;

	auto driver = _logger->trackedIndexes().value(index.row());
	auto variable = _logger->variables().value(index.column());

	switch(role) {
		case Qt::DisplayRole:
			data = _logger->loggedData(driver, variable);
			break;
		default:
			break;
	}

	return data;
}


QVariant DebuggerTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	auto data = QVariant();

	if(role == Qt::DisplayRole) {
		if(orientation == Qt::Vertical) {
			data = _logger->trackedNames().value(section);
		} else {
			auto variable = _logger->variables().value(section);
			data = _logger->variableName(variable);
		}
	}

	return data;
}

void DebuggerTableModel::refresh()
{
	if(_nbLoadedRows != rowCount() || _nbLoadedCols != columnCount()) {
		_nbLoadedRows = rowCount();
		_nbLoadedCols = columnCount();
		beginResetModel();
		endResetModel();
	} else {
		emit dataChanged(QModelIndex(), QModelIndex());
	}
}

UdpLogger *DebuggerTableModel::logger() const { return _logger; }
