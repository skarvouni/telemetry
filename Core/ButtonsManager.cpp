#include "ButtonsManager.h"
#include <QtDebug>

ButtonsManager::ButtonsManager()
{
	_pressed_func = [](auto b) { Q_UNUSED(b) };
	_released_func = [](auto b) { Q_UNUSED(b) };
}

void ButtonsManager::setButtonsStatus(int status)
{
	for(auto it = _pressed.constBegin(); it != _pressed.constEnd(); ++it) {
		auto button = it.key();
		auto pressed = (status & button) == button;
		if(pressed && !_pressed[button]) {
			_pressed_func(static_cast<Button>(button));
			_pressed[button] = true;
		} else if(!pressed && _pressed[button]) {
			_released_func(static_cast<Button>(button));
			_pressed[button] = false;
		}
	}
}

void ButtonsManager::trackButton(Button button)
{
	if(button != Button::NoButton) {
		_pressed[(int)button] = false;
	}
}

void ButtonsManager::untrackButton(Button button) { _pressed.remove((int)button); }

void ButtonsManager::clearTrackedButtons() { _pressed.clear(); }

void ButtonsManager::setPressed_callback(const std::function<void(Button)> &callback) { _pressed_func = callback; }

void ButtonsManager::setReleased_callback(const std::function<void(Button)> &callback) { _released_func = callback; }
